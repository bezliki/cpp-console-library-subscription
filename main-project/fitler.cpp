#include "filter.h"
#include <cstring>
#include <iostream>
marafonresults** filter(marafonresults* array[], int size, bool (*check)(marafonresults* element), int& result_size);
{
	marafonresults** result = new marafonresults * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_marafonresults_by_participant(marafonresults* element)
{
	return strcmp(element->author.first_name, "�������") == 0 &&
		strcmp(element->author.middle_name, "�������") == 0 &&
		strcmp(element->author.last_name, "�������") == 0;
}

bool check_marafonresults_by_time(marafonresults* element)
{
	return element->start.minute == 00 && element->start.second == 00;
}
