#pragma once
#ifndef MARAFON_RESULTS_H
#define MARAFON_RESULTS_H

#include "constants.h"

struct date
{
    int hour;
    int minute;
    int second;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct marafonresults
{
    person participant;
    date start;
    date finish;
    int nomer;
};

#endif
