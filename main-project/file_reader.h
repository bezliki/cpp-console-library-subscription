#pragma once
#ifndef FILE_READER_H
#define FILE_READER_H

#include "marafonresults.h"

void read(const char* file_name, marafonresults* array[], int& size);

#endif
