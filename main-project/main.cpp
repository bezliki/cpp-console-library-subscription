#include <iostream>
#include <iomanip>
using namespace std;

#include "marafonresults.h"
#include "file_reader.h"
#include "constants.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �1. ���������� ��������\n";
    cout << "�����: ����� ���-���-��\n\n";
    cout << "����� ������: 22-��_����\n\n";
    marafonresults* results[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", results, size);
        cout << "***** ���������� �������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** ����� �������� **********/
            cout << "��������........: ";
            // ����� �������
            cout << results[i]->participant.last_name << " ";
            // �����  �����
            cout << results[i]->participant.first_name << ". ";
            // �����  ��������
            cout << results[i]->participant.middle_name << ".";
            cout << '\n';
            /********** ����� ����� **********/
  
            // ����� ������
            cout << '"' << results[i]->nomer << '"';
            cout << '\n';
            /********** ����� ���� ������ **********/
            // ����� ����
            cout << "����� ������.....: ";
            cout << setw(4) << setfill('0') << results[i]->start.hour << '-';
            // ����� ������
            cout << setw(2) << setfill('0') << results[i]->start.minute << '-';
            // ����� �������
            cout << setw(2) << setfill('0') << results[i]->start.second;
            cout << '\n';
            /********** ����� ���� �������� **********/
            // ����� ����
            cout << "����� ������...: ";
            cout << setw(4) << setfill('0') << results[i]->finish.hour << '-';
            // ����� ������
            cout << setw(2) << setfill('0') << results[i]->finish.minute << '-';
            // ����� �������
            cout << setw(2) << setfill('0') << results[i]->finish.second;
            cout << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete results[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
       